package fr.epsi.resellersapi;
import fr.epsi.resellersapi.Exceptions.InvalidRequestException;
import fr.epsi.resellersapi.Model.*;
import fr.epsi.resellersapi.Model.Order;
import fr.epsi.resellersapi.Repository.OrderRepository;
import fr.epsi.resellersapi.Service.OrderService;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.Mockito;

import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.web.client.RestTemplate;
import org.springframework.boot.test.context.SpringBootTest;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class orderServiceTests {
    @InjectMocks
    OrderService orderService;

    @Mock
    OrderRepository orderRepository;

    @Mock
    private RestTemplate restTemplate;

    Product PRODUCT_1 =  Product.builder().id("63a30331e9b5d515d5393333").libelle("Expresso").name("Ryan le brun").details(new Details("150","5 doses 5 gouts","green")).stock(140L).build();
    Product PRODUCT_2 = Product.builder().id("63a30331e9b5d515d5394444").libelle("Nespresso").name("Ryan le brun").details(new Details("120","2 doses 2 gouts","blue")).stock(14L).build();

    Order ORDER_1 = Order.builder().id("63a30331e9b5d515d5391111").customerId( "63a30331e9b5d515d53968b8").productsId(new ArrayList<>(Arrays.asList(PRODUCT_1, PRODUCT_2))).build();

    Order ORDER_2 = Order.builder().id("63a30331e9b5d515d5392222").customerId( "63a30331e9b5d515d53968b8").productsId(new ArrayList<>(Arrays.asList(PRODUCT_1))).build();

    @Test
    public void testGetOrders()
    {
        List<Order> orderList = new ArrayList<>(Arrays.asList(ORDER_1, ORDER_2));

        Mockito.when(orderRepository.findAll()).thenReturn(orderList);

        //test
        List<Order> result = orderService.getOrders();

        assertEquals(2, result.size());
        verify(orderRepository, times(1)).findAll();
    }

    @Test
    public void testGetOrderById()
    {
        Mockito.when(orderRepository.findById("63a30331e9b5d515d5391111")).thenReturn(Optional.of(ORDER_1));

        //test
        Order result = orderService.getOrderById("63a30331e9b5d515d5391111");

        assertEquals("63a30331e9b5d515d5391111", result.getId());
        assertEquals("63a30331e9b5d515d53968b8", result.getCustomerId());
        assertEquals("63a30331e9b5d515d5393333", result.getProductsId().get(0).getId());
        assertEquals("63a30331e9b5d515d5394444", result.getProductsId().get(1).getId());

    }

    @Test
    public void testGetOrderByIdNotFoundException() throws Exception {

        ResourceNotFoundException thrown = assertThrows(
                ResourceNotFoundException.class,
                () -> orderService.getOrderById("63a30331e9b5d515d53968b9"),
                "Order  not found!"
        );

        assertTrue(thrown.getMessage().contentEquals("Order  not found!"));
    }

    @Test
    public void testAddOrder() {
        Order order = Order.builder()
                .customerId("63a30331e9b5d515d53968b8")
                .productsId(new ArrayList<>(Arrays.asList(PRODUCT_1, PRODUCT_2)))
                .build();

        Mockito.when(orderRepository.save(order)).thenReturn(order);

        //test
        Order result = orderService.addOrder(order);

        assertEquals("63a30331e9b5d515d53968b8", result.getCustomerId());
        assertEquals("63a30331e9b5d515d5393333", result.getProductsId().get(0).getId());
        assertEquals("63a30331e9b5d515d5394444", result.getProductsId().get(1).getId());
        assertEquals(result.getCreatedAt(), result.getUpdatedAt());
    }

    @Test
    public void testUpdateOrder() {
        Order order = Order.builder()
                .id("63a30331e9b5d515d5391111")
                .customerId("63a30331e9b5d515d53968b8")
                .productsId(new ArrayList<>(Arrays.asList(PRODUCT_1, PRODUCT_2)))
                .build();

        Mockito.when(orderRepository.findById("63a30331e9b5d515d5391111")).thenReturn(Optional.ofNullable(order));
        Mockito.when(orderRepository.save(order)).thenReturn(order);

        //test
        Order result = orderService.updateOrder(order);

        assertEquals("63a30331e9b5d515d53968b8", result.getCustomerId());
        assertEquals("63a30331e9b5d515d5393333", result.getProductsId().get(0).getId());
        assertEquals("63a30331e9b5d515d5394444", result.getProductsId().get(1).getId());
        assertTrue(result.getCreatedAt() != result.getUpdatedAt());
    }

    @Test
    public void testUpdateOrderNotFoundException() throws Exception {

        Order order = Order.builder()
                .id("63a30331e9b5d515d5391856")
                .customerId("63a30331e9b5d515d53968b8")
                .productsId(new ArrayList<>(Arrays.asList(PRODUCT_1, PRODUCT_2)))
                .build();

        ResourceNotFoundException thrown = assertThrows(
                ResourceNotFoundException.class,
                () -> orderService.updateOrder(order),
                "Order with id 63a30331e9b5d515d5391856 not found!"
        );

        assertTrue(thrown.getMessage().contentEquals("Order with id 63a30331e9b5d515d5391856 not found!"));
    }

    @Test
    public void testUpdateOrderWithNullId() throws Exception {
        Order order = Order.builder()
                .id(null)
                .customerId("63a30331e9b5d515d53968b8")
                .productsId(new ArrayList<>(Arrays.asList(PRODUCT_1, PRODUCT_2)))
                .build();

        InvalidRequestException thrown = assertThrows(
                InvalidRequestException.class,
                () -> orderService.updateOrder(order),
                "Order or id must not be null!"
        );

        assertTrue(thrown.getMessage().contentEquals("Order or id must not be null!"));
    }

    @Test
    public void testUpdateOrderWithNullOrder() throws Exception {
        InvalidRequestException thrown = assertThrows(
                InvalidRequestException.class,
                () -> orderService.updateOrder(null),
                "Order or id must not be null!"
        );

        assertTrue(thrown.getMessage().contentEquals("Order or id must not be null!"));
    }

    @Test
    public void testDeleteOrder() throws Exception {
        Mockito.when(orderRepository.findById("63a30331e9b5d515d5391111")).thenReturn(Optional.of(ORDER_1));

        orderService.deleteOrder("63a30331e9b5d515d5391111");

        // verify the mocks
        verify(orderRepository, times(1)).deleteById(eq("63a30331e9b5d515d5391111"));

    }

    @Test
    public void testDeleteOrderNotFound() throws Exception {
        Mockito.when(orderRepository.findById("63a30331e9b5d515d5391156")).thenThrow(new ResourceNotFoundException("Order with id 63a30331e9b5d515d5391156 not found!"));
        ResourceNotFoundException thrown = assertThrows(
                ResourceNotFoundException.class,
                () -> orderService.deleteOrder("63a30331e9b5d515d5391156"),
                "Order with id 63a30331e9b5d515d5391156 not found!"
        );

        assertTrue(thrown.getMessage().contentEquals("Order with id 63a30331e9b5d515d5391156 not found!"));
    }


    @Test
    void testGetOrdersByCustomerId() {
        List<Order> orderList = new ArrayList<>(Arrays.asList(ORDER_1, ORDER_2));
        Mockito.when(orderRepository.findAllByCustomerId("63a30331e9b5d515d5391115")).thenReturn(orderList);

        //test
        List<Order> result = orderService.getOrdersByCustomerId("63a30331e9b5d515d5391115");

        assertEquals("63a30331e9b5d515d5391111", result.get(0).getId());
        assertEquals("63a30331e9b5d515d53968b8", result.get(0).getCustomerId());
        assertEquals("63a30331e9b5d515d5393333", result.get(0).getProductsId().get(0).getId());
        assertEquals("63a30331e9b5d515d5394444", result.get(0).getProductsId().get(1).getId());
        assertEquals("63a30331e9b5d515d5392222", result.get(1).getId());
        assertEquals("63a30331e9b5d515d53968b8", result.get(1).getCustomerId());
        assertEquals("63a30331e9b5d515d5393333", result.get(1).getProductsId().get(0).getId());


    }

    @Test
    void testGetOrdersByCustomerIdAndOrderId() {
        Mockito.when(orderRepository.findOrderByCustomerIdAndOrderId("63a30331e9b5d515d5391115","63a30331e9b5d515d5391111")).thenReturn(ORDER_1);

        //test
        Order result = orderService.getOrdersByCustomerIdAndOrderId("63a30331e9b5d515d5391115","63a30331e9b5d515d5391111");

        assertEquals("63a30331e9b5d515d5391111", result.getId());
        assertEquals("63a30331e9b5d515d53968b8", result.getCustomerId());
        assertEquals("63a30331e9b5d515d5393333", result.getProductsId().get(0).getId());
        assertEquals("63a30331e9b5d515d5394444", result.getProductsId().get(1).getId());



    }
}
