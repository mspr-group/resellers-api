package fr.epsi.resellersapi;

import fr.epsi.resellersapi.Exceptions.InvalidRequestException;
import fr.epsi.resellersapi.Model.Details;
import fr.epsi.resellersapi.Model.Order;
import fr.epsi.resellersapi.Model.Product;
import fr.epsi.resellersapi.Repository.OrderRepository;
import fr.epsi.resellersapi.Repository.ProductRepository;
import fr.epsi.resellersapi.Service.OrderService;
import fr.epsi.resellersapi.Service.ProductService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import org.mockito.Mockito;

import org.springframework.data.rest.webmvc.ResourceNotFoundException;

import org.springframework.web.client.RestTemplate;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.times;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;





import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;


@ExtendWith(MockitoExtension.class)
class ResellersapiApplicationTests {
	@InjectMocks
	ProductService productService;

	@Mock
	ProductRepository productRepository;

	@Mock
	OrderRepository orderRepository;
	@Mock
	OrderService orderService;

	@Mock
	private RestTemplate restTemplate;


	Product PRODUCT_1 =  Product.builder().id("1").libelle("Expresso").name("Ryan le brun").details(new Details("150","5 doses 5 gouts","green")).stock(140L).build();
	Product PRODUCT_2 = Product.builder().id("63a30331e9b5d515d53968b8").libelle("Nespresso").name("Ryan le brun").details(new Details("120","2 doses 2 gouts","blue")).stock(14L).build();
	Product PRODUCT_3 = Product.builder().id("3").libelle("Caps").name("Ryan le brun").details(new Details("110","3 doses 3 gouts","red")).stock(19L).build();
	Product PRODUCT_4 = Product.builder().id("4").libelle("Filtre").name("Ryan le brun").details(new Details("100","6 doses 6 gouts","yellow")).stock(70L).build();

	Order ORDER_1 = Order.builder().id("63a30331e9b5d515d5391111").customerId( "63a30331e9b5d515d53968a5").productsId(new ArrayList<>(Arrays.asList(PRODUCT_1, PRODUCT_2))).build();



	@Test
	public void testGetProducts()
	{
		List<Product> productList = new ArrayList<>(Arrays.asList(PRODUCT_1, PRODUCT_2));

		Mockito.when(productRepository.findAll()).thenReturn(productList);

		//test
		List<Product> result = productService.getProducts();

		assertEquals(2, result.size());
		verify(productRepository, times(1)).findAll();
	}

	@Test
	public void testGetProductById()
	{
		Mockito.when(productRepository.findById("63a30331e9b5d515d53968b8")).thenReturn(Optional.of(PRODUCT_2));

		//test
		Product result = productService.getProductById("63a30331e9b5d515d53968b8");

		assertEquals("63a30331e9b5d515d53968b8", result.getId());
		assertEquals("Nespresso", result.getLibelle());
		assertEquals("Ryan le brun", result.getName());
		assertEquals("2 doses 2 gouts", result.getDetails().getDescription());
		assertEquals(14L, result.getStock());
	}

	@Test
	public void testGetProductByIdNotFoundException() throws Exception {

		ResourceNotFoundException thrown = assertThrows(
				ResourceNotFoundException.class,
				() -> productService.getProductById("63a30331e9b5d515d53968b9"),
				"Product  not found!"
		);

		assertTrue(thrown.getMessage().contentEquals("Product  not found!"));
	}

	@Test
	public void testAddProduct() {
		Product product = Product.builder()
				.libelle("Expresso lavazza")
				.name("ELGHAZI youness")
				.details(new Details("120", "10 doses avec 10 gouts", "Black"))
				.stock(30L)
				.build();

		Mockito.when(productRepository.save(product)).thenReturn(product);

		//test
		Product result = productService.addProduct(product);

		assertEquals("Expresso lavazza", result.getLibelle());
		assertEquals("ELGHAZI youness", result.getName());
		assertEquals("120", result.getDetails().getPrice());
		assertEquals("10 doses avec 10 gouts", result.getDetails().getDescription());
		assertEquals("Black", result.getDetails().getColor());
		assertEquals(30L, result.getStock());
		assertEquals(result.getCreatedAt(), result.getUpdatedAt());
	}

	@Test
	public void testUpdateProduct() {
		Product product = Product.builder()
				.id("63a30331e9b5d515d53968b8")
				.libelle("Expresso lavazza 2022")
				.name("Ismail")
				.details(new Details("100", "10 doses avec 10 gouts", "Black"))
				.stock(18L)
				.build();

		Mockito.when(productRepository.findById("63a30331e9b5d515d53968b8")).thenReturn(Optional.ofNullable(product));
		Mockito.when(productRepository.save(product)).thenReturn(product);

		//test
		Product result = productService.updateProduct(product);

		assertEquals("Expresso lavazza 2022", result.getLibelle());
		assertEquals("Ismail", result.getName());
		assertEquals("100", result.getDetails().getPrice());
		assertEquals("10 doses avec 10 gouts", result.getDetails().getDescription());
		assertEquals("Black", result.getDetails().getColor());
		assertEquals(18L, result.getStock());
		assertTrue(result.getCreatedAt() != result.getUpdatedAt());
	}

	@Test
	public void testUpdateProductNotFoundException() throws Exception {

		Product product = Product.builder()
				.id("63a30331e9b5d515d5396845")
				.libelle("Expresso lavazza 2022")
				.name("Ismail")
				.details(new Details("100", "10 doses avec 10 gouts", "Black"))
				.stock(18L)
				.build();

		ResourceNotFoundException thrown = assertThrows(
				ResourceNotFoundException.class,
				() -> productService.updateProduct(product),
				"Product with id 63a30331e9b5d515d5396845 not found!"
		);

		assertTrue(thrown.getMessage().contentEquals("Product with id 63a30331e9b5d515d5396845 not found!"));
	}

	@Test
	public void testUpdateProductWithNullId() throws Exception {
		Product product = Product.builder()
				.id(null)
				.libelle("Expresso lavazza 2022")
				.name("Ismail")
				.details(new Details("100", "10 doses avec 10 gouts", "Black"))
				.stock(18L)
				.build();

		InvalidRequestException thrown = assertThrows(
				InvalidRequestException.class,
				() -> productService.updateProduct(product),
				"Product or id must not be null!"
		);

		assertTrue(thrown.getMessage().contentEquals("Product or id must not be null!"));
	}

	@Test
	public void testUpdateProductWithNullProduct() throws Exception {
		InvalidRequestException thrown = assertThrows(
				InvalidRequestException.class,
				() -> productService.updateProduct(null),
				"Product or id must not be null!"
		);

		assertTrue(thrown.getMessage().contentEquals("Product or id must not be null!"));
	}

	@Test
	public void testDeleteProduct() throws Exception {
		Mockito.when(productRepository.findById("63a30331e9b5d515d53968b8")).thenReturn(Optional.of(PRODUCT_1));

		productService.deleteProduct("63a30331e9b5d515d53968b8");

		// verify the mocks
		verify(productRepository, times(1)).deleteById(eq("63a30331e9b5d515d53968b8"));

	}

	@Test
	public void testDeleteProductNotFound() throws Exception {
		Mockito.when(productRepository.findById("63a30331e9b5d515d53968b4")).thenThrow(new ResourceNotFoundException("Product with id 63a30331e9b5d515d53968b4 not found!"));
		ResourceNotFoundException thrown = assertThrows(
				ResourceNotFoundException.class,
				() -> productService.deleteProduct("63a30331e9b5d515d53968b4"),
				"Product with id 63a30331e9b5d515d53968b4 not found!"
		);

		assertTrue(thrown.getMessage().contentEquals("Product with id 63a30331e9b5d515d53968b4 not found!"));
	}


	@Test
	public void testGetProductByIdFromOrder() throws Exception {
		//Mockito.when(productRepository.findById("63a30331e9b5d515d53968b8")).thenReturn(Optional.of(PRODUCT_2));

		//test
		Order orderResult = ORDER_1;
		Product result = productService.getProductByIdFromOrder("63a30331e9b5d515d53968b8", orderResult);


		assertEquals("63a30331e9b5d515d53968b8", result.getId());
		assertEquals("Nespresso", result.getLibelle());
		assertEquals("Ryan le brun", result.getName());
		assertEquals("2 doses 2 gouts", result.getDetails().getDescription());
		assertEquals(14L, result.getStock());
	}





}

