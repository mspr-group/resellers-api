package fr.epsi.resellersapi.Service;

import fr.epsi.resellersapi.Model.NotificationEmail;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.internet.*;
import java.util.Date;
import java.util.Properties;

@Service
public class MailService {

    private Multipart multipart;

    public MailService() {
        this.multipart = new MimeMultipart();
    }
    @Async
    public void sendMail(NotificationEmail notificationEmail) throws MessagingException {

        Message msg = new MimeMessage(setSession(setProperties()));
        setMessage(notificationEmail);
        msg.setContent(multipart);

        msg.setSentDate(new Date());
        msg.setSubject("You're subscribed on newsletter");

        msg.setFrom(new InternetAddress("no-replay@paytonkawa.com", false));
        msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(notificationEmail.getRecipient()));

        Transport.send(msg);
    }

    private void setMessage(NotificationEmail notificationEmail) throws MessagingException {
        MimeBodyPart message =  new MimeBodyPart();

        StringBuffer htmlContent = new StringBuffer();
        htmlContent.append("<html><head><style>");
        htmlContent.append("body { background-color: #f2f2f2; }");
        htmlContent.append("h1 { color: #333; }");
        htmlContent.append("</style></head><body><h1>");
        htmlContent.append(notificationEmail.getBody());
        htmlContent.append(", you're very welcome here! </h1>");
        String img = new String("<img src='data:image/png;base64,");
        String qrCode = notificationEmail.getQrCode();

        img = "<div>".concat(img).concat(qrCode).concat("'/></div>");
        System.out.println(img);
        htmlContent.append(img);
        htmlContent.append("<br /></body></html>");

        message.setContent(htmlContent.toString(), "text/html");

        multipart.addBodyPart(message);
    }

    private Session setSession(Properties props) {
        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("203228e68138a3", "0e15aff867dddf");
            }
        });
        return session;
    }

    private Properties setProperties() {
        Properties props = new Properties();

        props.put("mail.smtp.port", "2525");
        props.put("mail.smtp.host", "smtp.mailtrap.io");
        props.put("mail.smtp.auth", true);
        props.put("mail.smtp.starttls.enable", true);

        return props;
    }
}
