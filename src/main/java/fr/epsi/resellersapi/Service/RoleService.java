package fr.epsi.resellersapi.Service;

import fr.epsi.resellersapi.Model.Role;
import fr.epsi.resellersapi.Repository.RoleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class RoleService {
    private final RoleRepository roleRepository;

    public Role saveOrUpdate(Role role) {
        return roleRepository.save(role);
    }

    public Role getRole(String roleName) throws Exception {
        Role role = roleRepository.findByName(roleName).orElseThrow(() ->
                new Exception("Role not found with name - " + roleName));
        return role;
    }
}
