package fr.epsi.resellersapi.Model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "Order")
@Builder
public class Order {
    private @MongoId(FieldType.OBJECT_ID) String id;
    private String customerId;
    @DBRef
    private List<Product> productsId ;
    @CreatedDate @Builder.Default
    private Date createdAt = new Date();
    @LastModifiedDate
    @Builder.Default
    private Date updatedAt = new Date();
}
