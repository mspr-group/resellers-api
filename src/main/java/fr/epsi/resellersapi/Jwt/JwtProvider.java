package fr.epsi.resellersapi.Jwt;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Arrays.stream;

@Component
public class JwtProvider {


    @Value("${jwt.expiration.time}")
    private Long jwtExpirationInMillis;

    private String jwtSecrete = "secreteKey";

    private Algorithm algorithm = Algorithm.HMAC256(jwtSecrete.getBytes());

    public String generateToken(User user){
        List <String> claims = getClaimFromUser(user);
        String access_token = JWT.create()
                .withSubject(user.getUsername())
                .withIssuedAt(Instant.now())
                .withExpiresAt(Instant.now().plusMillis(jwtExpirationInMillis))
                .withClaim("roles", claims)
                .sign(algorithm);
        return access_token;
    }

    public String generateTokenQR(fr.epsi.resellersapi.Model.User user){
        List <String> claims = user.getAutorities().stream().map(obj -> obj.toString()).collect(Collectors.toList());
        String access_token = JWT.create()
                .withSubject(user.getEmail())
                .withIssuedAt(Instant.now())
                .withExpiresAt(Instant.now().plusMillis(jwtExpirationInMillis))
                .withClaim("roles", claims)
                .sign(algorithm);
        return access_token;
    }

    public List<GrantedAuthority> getAuthorities(String token){
        String[] claims = getCLaimsFromToken(token);
        return stream(claims).map(SimpleGrantedAuthority::new).collect(Collectors.toList());
    }

    public Authentication getAuthentication(String email, List<GrantedAuthority> authorities, HttpServletRequest request) {
        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(email, null, authorities);
        authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
        //SecurityContextHolder.getContext().setAuthentication(authenticationToken);
        return authenticationToken;
    }

    public boolean isTokenValid(String token) {
        JWTVerifier verifier = getJWTVerifier();
        return isTokenExpired(verifier, token);
    }

    public String getSubject(String token) {
        JWTVerifier verifier = getJWTVerifier();
        return verifier.verify(token).getSubject();
    }

    private boolean isTokenExpired(JWTVerifier verifier, String token) {
        Instant expiration = verifier.verify(token).getExpiresAtAsInstant();
        return !expiration.isBefore(new Date().toInstant());
    }

    private String[] getCLaimsFromToken(String token) {
        JWTVerifier verifier = getJWTVerifier();
        return verifier.verify(token).getClaim("roles").asArray(String.class);
    }

    private JWTVerifier getJWTVerifier() {
        JWTVerifier verifier;
        try{
            verifier = JWT.require(algorithm).build();
        } catch (JWTVerificationException ex) {
            throw new JWTVerificationException("TOKEN CANNOT BE VERIFIED");
        }
        return verifier;
    }

    private List <String> getClaimFromUser(User user) {
        return user.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList());
    }
}
