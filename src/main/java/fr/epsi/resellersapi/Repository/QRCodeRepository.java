package fr.epsi.resellersapi.Repository;

import fr.epsi.resellersapi.Model.QRCode;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface QRCodeRepository extends MongoRepository<QRCode, String> {
}
