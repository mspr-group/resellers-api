package fr.epsi.resellersapi.Repository;
import fr.epsi.resellersapi.Model.Product;
import org.springframework.data.mongodb.repository.MongoRepository;


public interface ProductRepository extends MongoRepository<Product, String> {

}
