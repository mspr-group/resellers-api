package fr.epsi.resellersapi.Repository;

import fr.epsi.resellersapi.Model.Role;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface RoleRepository extends MongoRepository <Role, String> {
    Optional<Role> findByName(String roleName);
}
